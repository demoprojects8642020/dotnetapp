variable "docker_image" {
  description = "The Docker image to deploy"
  type        = string
}

variable "helm_chart_path" {
  description = "The Helm chart path"
  type        = string
}

variable "namespace" {
  description = "Environment for deployment"
  type        = string
}
