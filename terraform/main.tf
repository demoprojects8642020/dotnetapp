resource "helm_release" "release" {
  name       = "dotnetapp1"
  chart      = var.helm_chart_path
  version    = "1.0.0"
  namespace  = var.namespace

  set {
    name  = "image.repository"
    value = var.docker_image
  }
}
